//
//  PlacesPickerViewController.swift
//  testAssignmentGMSPlaces
//
//  Created by Sachin Ambegave on 21/03/19.
//  Copyright © 2019 Sachin Ambegave. All rights reserved.
//

import UIKit
import GooglePlaces


protocol SelectLocationDelegate {
    func didChangeLocations(locations: NSDictionary)
}

class PlacesPickerViewController: UIViewController {
    
    @IBOutlet weak var fromTextField: UITextField!
    @IBOutlet weak var fromLabel: UILabel!
    @IBOutlet weak var toTextField: UITextField!
    @IBOutlet weak var toLabel: UILabel!
    lazy var isFrom: Bool = false
    var locations = NSMutableDictionary()
    var locationDelegate: SelectLocationDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
    }
    
    func showAlert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .cancel) { (cancel) in
            
        }
        alertController.addAction(action)
        self.present(alertController, animated: true) {
            
        }
    }
    
    @IBAction func showRouteButtonAction(_ sender: UIButton!) {
        
        if let from = locations.value(forKey: "FromDict") as? NSArray {
            if let fromDict = from.firstObject as? NSDictionary {
                if let _ = fromDict.value(forKey: "Latitude"), let _ = fromDict.value(forKey: "Longitude") {
                    
                }else {
                    self.showAlert(title: "Select From Location", message: "Please select from location to continue!")
                }
            }else {
                self.showAlert(title: "Select From Location", message: "Please select from location to continue!")
            }
        }else {
            self.showAlert(title: "Select From Location", message: "Please select from location to continue!")
        }

        if let from = locations.value(forKey: "ToDict") as? NSArray {
            if let fromDict = from.firstObject as? NSDictionary {
                if let _ = fromDict.value(forKey: "Latitude"), let _ = fromDict.value(forKey: "Longitude") {
                    
                }else {
                    self.showAlert(title: "Select To Location", message: "Please select to location to continue!")
                }
            }else {
                self.showAlert(title: "Select To Location", message: "Please select to location to continue!")
            }
        }else {
            self.showAlert(title: "Select To Location", message: "Please select to location to continue!")
        }

        
        locationDelegate?.didChangeLocations(locations: locations)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func getFromAddressButtonAction(_ sender: UIButton!) {
        isFrom = true
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
        
    }
    
    @IBAction func getToAddressButtonAction(_ sender: UIButton!) {
        isFrom = false
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
        
    }
}

extension PlacesPickerViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == fromTextField {
            fromTextField.resignFirstResponder()
        }else if textField == toTextField {
            toTextField.resignFirstResponder()
        }
        return true
    }
}

extension PlacesPickerViewController : GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        var address = ""
        if let addressStr = place.formattedAddress {
            address = addressStr
        } else { address = "" }
        
        var name = ""
        if let nameStr = place.formattedAddress {
            name = nameStr
        } else { name = "" }
        
        if isFrom {

            let dict = NSMutableDictionary()
            dict.setValue(name, forKey: "name")
            dict.setValue(place.coordinate.latitude, forKey: "Latitude")
            dict.setValue(place.coordinate.longitude, forKey: "Longitude")
            locations.setValue([dict], forKey: "FromDict")
            
            self.fromTextField.text = "\(name), \(address)"
            
            self.fromLabel.text = """
            Place name:\(name),
            Place Latitude:\(place.coordinate.latitude),
            Place Longitude:\(place.coordinate.longitude)
            """
        }else {
            
            let dict = NSMutableDictionary()
            dict.setValue(name, forKey: "name")
            dict.setValue(place.coordinate.latitude, forKey: "Latitude")
            dict.setValue(place.coordinate.longitude, forKey: "Longitude")
            locations.setValue([dict], forKey: "ToDict")
            
            self.toTextField.text = "\(name), \(address)"
            
            self.toLabel.text = """
            Place name:\(name),
            Place Latitude:\(place.coordinate.latitude),
            Place Longitude:\(place.coordinate.longitude)
            """
        }
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
