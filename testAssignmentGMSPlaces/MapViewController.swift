//
//  ViewController.swift
//  testAssignmentGMSPlaces
//
//  Created by Sachin Ambegave on 20/03/19.
//  Copyright © 2019 Sachin Ambegave. All rights reserved.
//

import UIKit
import GoogleMaps
import ARCarMovement

class MapViewController: UIViewController {
    
    static let initialString = "Get Locations To Show Route"
    static let changeLocationString = "Change Locations"
    
    @IBOutlet weak var selectLocationsButton: UIButton!
    @IBOutlet weak var mapView: GMSMapView!
    var moveMent: ARCarMovement?
    
    var coordinateArr = NSMutableArray()
    var oldCoordinate: CLLocationCoordinate2D?
    weak var timer: Timer?
    var counter: Int = 0
    
    
    lazy var driverMarker = GMSMarker()
    
    let pointsJSON = ["points":"}zepBojkaM`D\\xEj@xHtAt@LhBPpDR~AFrCVBCLE@?D@B@DJALGJA@Kj@m@tDe@fEe@xGO~Bg@pB@?@B@DAJEBA?A?o@lFg@dGg@nE@@?@@BAFCBA@S`Bm@lCoArFuAfF}C`Ok@bCO^a@|@IJ@l@Bn@CFs@v@m@QKC_@F{Bb@IYeBd@yGdBeJ|B{^fJ_B^iKhCiTlFgFtAyObEqCv@kDdAiGbB_NrDo@T}I~BoFvA{Y`IoEfAmL`D}OjEwLbDe@LMNm@ZkAh@sF`BOg@Me@x@WzDeA|@Mx@C~H{BpNwDzS}Fpu@iSNCJ]r@_@d@Wr@SlEqA"]
    
    lazy var routeJSON = [String: Any]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        routeJSON = ["overview_polyline":pointsJSON]
        mapView.delegate = self
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        selectLocationsButton.setTitle(MapViewController.initialString, for: .normal)
        driverMarker = GMSMarker()
        moveMent = ARCarMovement()
        moveMent?.delegate = self
        
        //alloc array and load coordinate from json file
        //
        var filePath = Bundle.main.path(forResource: "coordinates", ofType: "json")
        var jsonData = NSData(contentsOfFile: filePath ?? "") as Data?
        if let jsonData = jsonData, let json = try? JSONSerialization.jsonObject(with: jsonData, options: []) as? [Any] {
            coordinateArr.addObjects(from: (json as! NSArray) as! [Any])
        }
        
        oldCoordinate = CLLocationCoordinate2DMake(18.5491105, 73.7912815)
        driverMarker.icon = UIImage(named: "car")
        driverMarker.map = self.mapView
        driverMarker.position = self.oldCoordinate!
        
        counter = 0
        
        timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.timerTriggered), userInfo: nil, repeats: true)
        /// THIS IS JUST TO DISPLAY THE POLYLINE ON MAP, GIVEN STATIC POLYLINE
//        let points = "}zepBojkaM}`C|}C"
//        DispatchQueue.main.async {
//            let camera = GMSCameraPosition.camera(withLatitude: 18.5491105, longitude: 73.7912815, zoom: 14.0)
//            self.mapView.camera = camera
//            self.mapView.animate(to: camera)
//            let path = GMSPath.init(fromEncodedPath: points)
//            let polyline = GMSPolyline.init(path: path)
//            polyline.strokeWidth = 4
//            polyline.strokeColor =  UIColor.black
//            polyline.map = self.mapView
//        }
        
        
        let routeOverviewPolyline = self.routeJSON["overview_polyline"] as! [String: Any]
        let points = routeOverviewPolyline["points"] as! String
        DispatchQueue.main.async {
            let camera = GMSCameraPosition.camera(withLatitude: 18.5491105, longitude: 73.7912815, zoom: 14.0)
            self.mapView.camera = camera
            self.mapView.animate(to: camera)
            let path = GMSPath.init(fromEncodedPath: points)
            let polyline = GMSPolyline.init(path: path)
            polyline.strokeWidth = 4
            polyline.strokeColor =  UIColor.black
            polyline.map = self.mapView
        }
        ///
    }
    
    @objc
    func timerTriggered() {
        
        if counter < coordinateArr.count {
            
//            let newCoordinate: CLLocationCoordinate2D = CLLocationCoordinate2DMake(coordinateArr[counter]["lat"].floatValue, coordinateArr[counter]["long"].floatValue)

            var newCoordinate: CLLocationCoordinate2D = CLLocationCoordinate2DMake((coordinateArr[counter] as! NSDictionary).value(forKey: "lat") as! Double, (coordinateArr[counter] as! NSDictionary).value(forKey: "long") as! Double)
            /**
             *  You need to pass the created/updating marker, old & new coordinate, mapView and bearing value from backend
             *  to turn properly. Here coordinates json files is used without new bearing value. So that
             *  bearing won't work as expected.
             */
            moveMent?.ARCarMovement(marker: driverMarker, oldCoordinate: oldCoordinate!, newCoordinate: newCoordinate, mapView: self.mapView, bearing: 0)
            
            oldCoordinate = newCoordinate
    
            self.counter += 1
        }else {
            self.timer?.invalidate()
            self.timer = nil
        }
    }
    
    @IBAction func selectLocationsButtonAction(_ sender: UIButton!) {
        
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "PlacesPickerViewController") as? PlacesPickerViewController else { return }
        vc.locationDelegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }

//    func createCarMarker(at latitude: Double, longitude: Double) {
//        // I have taken a pin image which is a custom image
//        let markerImage = UIImage(named: "car")
//
//        //creating a marker view
//        let markerView = UIImageView(image: markerImage)
//
//        //changing the tint color of the image
//        markerView.tintColor = UIColor.red
//
//        carMarker.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
//        let camera = GMSCameraPosition.camera(withLatitude: latitude, longitude: longitude, zoom: 14.0)
//        self.mapView.camera = camera
//        self.mapView.animate(to: camera)
//        carMarker.iconView = markerView
//        carMarker.title = "New Delhi"
//        carMarker.snippet = "India"
//        carMarker.map = mapView
//
//        //comment this line if you don't wish to put a callout bubble
//        mapView.selectedMarker = carMarker
//    }
    
    
    /// DRAW POLYLINE
    func drawPath(startLocation: CLLocation, endLocation: CLLocation, locations: NSDictionary) {
        
        let origin = "\(startLocation.coordinate.latitude),\(startLocation.coordinate.longitude)"
        let destination = "\(endLocation.coordinate.latitude),\(endLocation.coordinate.longitude)"
        let urlStr = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&key=\((GMSKey.apiKey))"
        
        guard let url = URL(string: urlStr) else { return }
        
        var urlRequest = URLRequest(url: url)
        
        urlRequest.httpMethod = "GET"

        // set up the session
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let task = session.dataTask(with: urlRequest) { (data, response, error) in
            guard let response = response, let data = data else { return }
            print("responseCode: \(String(describing: (response as? HTTPURLResponse)?.statusCode))")
            if error == nil {
                guard let json = (try? JSONSerialization.jsonObject(with: data, options: .mutableLeaves)) as? [String: Any] else {
                    print("Not containing JSON")
                    return
                }
                print("JSON : \n \(json)")

                let routes = json["routes"] as! NSArray

                
                let routeOverviewPolyline = self.routeJSON["overview_polyline"] as! [String: Any]
                let points = routeOverviewPolyline["points"] as! String
                DispatchQueue.main.async {
                    let path = GMSPath.init(fromEncodedPath: points)
                    let polyline = GMSPolyline.init(path: path)
                    polyline.strokeWidth = 4
                    polyline.strokeColor =  UIColor.black
                    polyline.map = self.mapView
                }
                
//                for route in routes {
//                    let routeOverviewPolyline = (route as! NSDictionary)["overview_polyline"] as! NSDictionary
//                    let points = routeOverviewPolyline["points"] as! String
//                    DispatchQueue.main.async {
//                        let path = GMSPath.init(fromEncodedPath: points)
//                        let polyline = GMSPolyline.init(path: path)
//                        polyline.strokeWidth = 4
//                        polyline.strokeColor =  UIColor.black
//                        polyline.map = self.mapView
//                    }
//                }
            }
        }

        task.resume()

        self.showMapMarker(from: locations)
    }
    
    
    /// MARK:- Show Map Marker
    internal func showMapMarker(from locations: NSDictionary) {
        
        let fromArray : NSArray =  locations.value(forKey: "FromDict") as! NSArray
        let toArray : NSArray = locations.value(forKey: "ToDict") as! NSArray
        
        let markersArray : NSMutableArray = NSMutableArray()
        
        self.mapView.clear()
        
        /// FROM LOCATION
        let fromDict = fromArray.firstObject as! NSDictionary
        
        let fromLat = fromDict.value(forKey: "Latitude") as! Double
        let fromLong = fromDict.value(forKey: "Longitude") as! Double
        let fromName = fromDict.value(forKey: "name") as! String
        
        var latFloatValue : Double = fromLat
        var longFloatValue : Double = fromLong
        var name : String = NSString(string: fromName) as String
        
        var marker : GMSMarker = GMSMarker()
        marker.position = CLLocationCoordinate2D (latitude: latFloatValue, longitude: longFloatValue)
        marker.title = String(format: "%@",name)
        marker.map = self.mapView
        markersArray.add(marker)
        
        /// TO LOCATION
        let toDict = toArray.firstObject as! NSDictionary
        
        let toLat = toDict.value(forKey: "Latitude") as! Double
        let toLong = toDict.value(forKey: "Longitude") as! Double
        let toName = toDict.value(forKey: "name") as! String
        
        latFloatValue = toLat
        longFloatValue = toLong
        name = NSString(string: toName) as String
        
        marker = GMSMarker()
        marker.position = CLLocationCoordinate2D (latitude: latFloatValue, longitude: longFloatValue)
        marker.title = String(format: "%@",name)
        marker.map = self.mapView
        markersArray.add(marker)
        
        let firstLocation: CLLocationCoordinate2D = (markersArray.firstObject as! GMSMarker).position
        
        var bounds: GMSCoordinateBounds = GMSCoordinateBounds(coordinate: firstLocation, coordinate: firstLocation)
        
        for (_, element) in markersArray.enumerated() {

            var marker: GMSMarker = GMSMarker ()
            marker = element as! GMSMarker
            bounds = bounds.includingCoordinate(marker.position)
        }
        
        CATransaction.begin()
        mapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 50.0))
        CATransaction.commit()
    }
}

extension MapViewController: SelectLocationDelegate {
    
    func didChangeLocations(locations: NSDictionary) {
        print("Did return to mapView with \n locations: \(locations)")
        let fromArray : NSArray =  locations.value(forKey: "FromDict") as! NSArray
        let toArray : NSArray = locations.value(forKey: "ToDict") as! NSArray
        
        /// FROM LOCATION
        let fromDict = fromArray.firstObject as! NSDictionary
        
        let sourceLat = fromDict.value(forKey: "Latitude") as! Double
        let sourceLong = fromDict.value(forKey: "Longitude") as! Double
        
        
        /// TO LOCATION
        let toDict = toArray.firstObject as! NSDictionary
        
        let destinationLat = toDict.value(forKey: "Latitude") as! Double
        let destinationLong = toDict.value(forKey: "Longitude") as! Double
        
        let startLOC = CLLocation(latitude: sourceLat, longitude: sourceLong)
        let endLOC = CLLocation(latitude: destinationLat, longitude: destinationLong)
        
        let camera = GMSCameraPosition.camera(withLatitude: sourceLat, longitude: sourceLong, zoom: 14.0)
        
        DispatchQueue.main.async {
            self.mapView.camera = camera
            self.mapView.animate(to: camera)
            self.drawPath(startLocation: startLOC, endLocation: endLOC, locations: locations)
            self.selectLocationsButton.setTitle(MapViewController.changeLocationString, for: .normal)
        }
    }
}

extension MapViewController: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
//        driverMarker.position = position.target
    }
}


extension MapViewController: ARCarMovementDelegate {
    func ARCarMovementMoved(_ Marker: GMSMarker) {
        
    }
    
    
}


struct GMSKey {
    static let apiKey = "AIzaSyD7HSqfW81HKtPCq6oVhnAvP-XfqR7CFus"
//    static let apiKey = "AIzaSyD53uE1nB6Ty43PbTXpnVbF7qgAZqf_KEA"
    
}
